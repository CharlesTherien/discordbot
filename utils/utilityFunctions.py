from secret.secrets import identities


def getHouseID(house):
    return identities["houses"][house.lower().capitalize()]

