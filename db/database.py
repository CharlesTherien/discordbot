import sqlite3
from discord import Member, Role
from secret.secrets import identities

class Database:
    path = "./db/databaseTest.sqlite"

    createUserTable = """CREATE TABLE IF NOT EXISTS users (
    id integer PRIMARY KEY,
    username text NOT NULL,
    score integer NOT NULL,
    botNick text,
    house text,
    pronoun integer
);"""

    def __init__(self):
        self.connection = sqlite3.connect(self.path)
        self.connection.row_factory = sqlite3.Row
        self.cursor = self.connection.cursor()
        self.cursor.execute(self.createUserTable)

    def disconnect(self):
        if self.connection is not None:
            self.connection.close()
            self.connection = None

    def add_user(self, member: Member):
        create_user = '''INSERT OR IGNORE INTO USERS(id, username, score, botNick, house, pronoun) 
        VALUES(?,?,?,?,?,?)'''
        self.cursor.execute(create_user, (member.id, member.name, 0, member.name, "", -1))
        self.connection.commit()

    def update_houses(self, member: Member):
        update_role = '''INSERT INTO USERS(id, username, score, botNick, house, pronoun)
        VALUES(?,?,?,?,?,?)
        ON CONFLICT(id) DO UPDATE SET house=?'''
        for m in member:
            if not m.bot:
                hoist_role = [x for x in m.roles if x.hoist][0].name
                self.cursor.execute(update_role, (m.id, m.name, 0, m.name, hoist_role, -1, hoist_role))
        self.connection.commit()

    def change_house(self, member: Member, house):
        update_role = '''INSERT INTO USERS(id, username, score, botNick, house, pronoun)
        VALUES(?,?,?,?,?,?)
        ON CONFLICT(id) DO UPDATE SET house=?'''
        self.cursor.execute(update_role, (member.id, member.name, 0, member.name, house, -1, house))
        self.connection.commit()

    def change_pronoun(self, member: Member, pronounNum):
        update_role = '''INSERT INTO USERS(id, username, score, botNick, house, pronoun)
        VALUES(?,?,?,?,?,?)
        ON CONFLICT(id) DO UPDATE SET pronoun=?'''
        self.cursor.execute(update_role, (member.id, member.name, 0, member.name,
                                          [x for x in member.roles if x.hoist][0].name, pronounNum, pronounNum))
        self.connection.commit()
