from discord.ext import commands
import logging

from secret.secrets import identities
from db.database import Database


class Admin(commands.Cog):
    def __init__(self, bot: commands.Bot, db: Database, log: logging.Logger = None):
        self.bot = bot
        self.db = db
        self.logger = log

    async def cog_check(self, ctx: commands.Context):
        return "Admin" in [x.name for x in ctx.author.roles]

    async def cog_command_error(self, ctx: commands.Context, error: commands.CommandError):
        if self.logger is not None:
            self.logger.log(logging.WARNING, f"{ctx.author.name} has caused {error.args}")

    @commands.command(name="majRoles")
    async def majRole(self, ctx: commands.Context):
        users = self.bot.get_guild(identities["guild"]).members
        self.db.update_houses(users)
        await ctx.send(f"there's {len(users)} users that the bot can see")

    @commands.command(name="debug", description="Not for playing with, users beware")
    async def debug(self, ctx: commands.Context):
        await ctx.send("Tadah!")
