#!/usr/bin/env python3

import logging
import discord
from discord.ext import commands
import random

from secret.secrets import get_token
from db.database import Database
from Cogs import Admin, RoleHandling
from utils.diceParser import DiceParser


logger = logging.getLogger("discord.bot")

bot = commands.Bot(command_prefix="!", fetch_online_members=False, case_insensitive=True,
                   activity=discord.Activity(type=discord.ActivityType.watching, name="for !help"))

db = Database()


def start_logging():
    syslogger = logging.getLogger('discord')
    syslogger.setLevel(logging.WARNING)
    handler = logging.FileHandler(filename="./logs/discordSys.log", encoding='utf-8', mode='a')
    formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s')
    handler.setFormatter(formatter)
    syslogger.addHandler(handler)
    logger.setLevel(logging.INFO)
    handler = logging.FileHandler(filename="./logs/discordClient.log", encoding='utf-8', mode='a')
    handler.setFormatter(formatter)
    logger.addHandler(handler)


async def send_dm(member: discord.Member, content):
    channel = member.dm_channel
    if channel is None:
        channel = await member.create_dm()
    await channel.send(content)


@bot.event
async def on_ready():
    print(f'{bot.user} has connected to discord!')
    start_logging()


@bot.event
async def on_member_join(member):
    logger.log(logging.INFO, f"{member.name} has joined")
    db.add_user(member)
    await send_dm(member, "Welcome to Bibtown!")


@bot.command(name='say', help="Makes the bot say something.")
async def say(ctx: commands.Context, *, text: str):
    member = ctx.author
    logger.log(logging.INFO, f"{member.name}({member.id}):{text}")
    await ctx.send(text)


@bot.command(name="ping", description="pong!",
             brief="pong!")
async def ping(ctx: commands.Context):
    await ctx.send("Pong!")


@bot.command(name="choose", description="When you can't pick, Bot can do for you!",
             brief="When you can't pick, Bot can do for you!")
async def choose(ctx: commands.Context, *choices: str):
    await ctx.send(random.choice(choices))


@bot.command(name="roll", brief="do all the fancy dice rolling you want!",
             help="""to roll dices, specify the dices as such: 
             "AdB" where A is the number of dice and B the size of the dice. 
             
             You can then modify the total by +,- and/or * to it, respecting order of operation and parenthesis:
             
             for example: roll 2d20+4 will roll 2 20sided dice and add the results together with 4.
             Complexier rolls are also valid: 8 + 2*3d8 - 1d13 is valid
             """)
async def roll(ctx: commands.Context, *, text: str):
    await ctx.send("You rolled: " + str(DiceParser().parse_expr(text)[0]))


if __name__ == '__main__':
    bot.add_cog(Admin.Admin(bot, db, logger))
    bot.add_cog(RoleHandling.RoleHandling(bot, db))
    bot.run(get_token())
