from discord.ext import commands
from discord.utils import get
from db.database import Database
from utils.utilityFunctions import getHouseID
from secret.secrets import identities


class RoleHandling(commands.Cog):
    def __init__(self, bot: commands.Bot, db: Database):
        self.bot = bot
        self.db = db

    @commands.command(name="hat", description="Call upon the hat to change houses!",
                      brief="Call upon the hat to change houses!")
    async def hat(self, ctx: commands.Context, *, house: str):
        try:
            house = getHouseID(house)
        except KeyError:
            await ctx.send("I did not understand which house you wanted :(")
            return
        role = get(ctx.guild.roles, id=house)

        self.db.change_house(ctx.author, role.name)
        role_remove = [x for x in ctx.author.roles if x.hoist]

        for r in role_remove:
            await ctx.author.remove_roles(r)
        await ctx.author.add_roles(role)
        await ctx.send(identities["welcomingMessages"][role.name])

    async def cog_command_error(self, ctx: commands.Context, error: commands.CommandError):
        await ctx.send_help(ctx.command.name)

    @commands.command(name="pronoun", description="To get a role denoting your pronouns!", help="""
    To select your pronouns, please answer with "!pronoun #" where # is the number of your choice:          
        
        1. He
        2. She
        3. They
        4. She/They
        5. He/They
        6. Any 
        7. Ey
        8. Xe
        9. Ae
         
    If your prefered pronouns aren't on the list, I'm sorry. ping @ssalogel and they'll fix it!
    """, brief="To select your pronoun(s)")
    async def pronoun(self, ctx: commands.Context, pronounNumber):
        if pronounNumber in identities["pronouns"].keys():
            pronounID = identities["pronouns"][pronounNumber]
            role_remove = [x for x in ctx.author.roles if x.id in identities["pronouns"].values()]
            role = get(ctx.guild.roles, id=pronounID)
            self.db.change_pronoun(ctx.author, pronounNumber)
            for r in role_remove:
                await ctx.author.remove_roles(r)
            await ctx.author.add_roles(role)
        else:
            await ctx.send_help()
